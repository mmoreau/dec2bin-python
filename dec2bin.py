#!/usr/bin/python3

class Conv:

	@classmethod
	def dec2bin(cls, n):

		""" Base 10 to Base 2. """

		if isinstance(n, int):

			bitLength = n.bit_length()
			_bin = ""
			switch = False

			if n < 0:
				switch = True
				n = ((2 ** bitLength) + n)

			if n == 0:
				_bin = "0b0"

			if n > 0:
				while n > 0:
					_bin += str(n % 2)
					n >>= 1

				if switch:
					_bin += ((bitLength - len(_bin)) * ["0", "1"][n % 2])
					_bin += ["0b0", "1b1"][((n + 1) % 2)]

				if not switch:
					_bin += ["b0", "b1"][n % 2]

			return _bin[::-1]