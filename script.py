#!/usr/bin/python3

import os
import sys
import random
from uuid import uuid4
from dec2bin import Conv


class Apps:


	@classmethod
	def way1(cls):

		sys.stdout.write(".:: Way n°1 - Random ::.\n\n")

		for _ in range(0, 3):
			n = random.randint(-sys.maxsize-1, sys.maxsize) # n = random.randint(-153, 182)
			sys.stdout.write("{} => {}\n".format(n, Conv.dec2bin(n)))

		sys.stdout.write("\n{}\n\n".format("-"*50))



	@classmethod
	def way2(cls):

		sys.stdout.write(".:: Way n°2 - Array ::.\n\n")

		arr = [-1, 3, 10, -30, sys.maxsize]

		for n in arr:
			sys.stdout.write("{} => {}\n".format(n, Conv.dec2bin(n)))

		sys.stdout.write("\n{}\n\n".format("-"*50))



	@classmethod
	def way3(cls):

		sys.stdout.write(".:: Way n°3 - File ::.\n\n")

		"""
			uuid4 = Universally Unique IDentifiers version 4

			in this function, uuid4 allows us to have a unique file name and avoids deleting or overwriting the contents 
			of the non-unique file. 

			Imagine that you have a file named "data.txt", in this file "data.txt" you have a lot of important content that 
			has absolutely nothing to do with the program, if the program had used a non-unique file name like "data.txt" 
			for example and you have a file "data.txt" where it finds the "script.py", when opening this file "data.txt" 
			in "w" mode (w = write), you will lose all data since the program will overwrite all the contents of the file 
			"data.txt", so we will use unique filenames, such as hash, uuid, guid.
		"""

		filename = "Dec2Bin-{}.txt".format(uuid4())

		sys.stdout.write("Filename : {}\n\n".format(filename))

		with open(filename, "w") as f:
			for _ in range(0, 3):
				n = random.randint(-sys.maxsize-1, sys.maxsize) # n = random.randint(-100, 100)
				f.write("{} => {}\n".format(n, Conv.dec2bin(n)))

		with open(filename, "r") as f:
			sys.stdout.write("{}".format(f.readlines()))

		sys.stdout.write("\n{}\n\n".format("-"*50))



	@classmethod
	def way4(cls):
		sys.stdout.write(".:: Way n°4 - Slice ::.\n\n")

		n = 24
		r = Conv.dec2bin(n)

		sys.stdout.write("By default : {}\n".format(r)) # By default
		sys.stdout.write("Get the sign : {}\n".format(r[0])) # Get the sign 
		sys.stdout.write("Get the binary value : {}\n".format(r[2:])) # Get the binary value
		sys.stdout.write("Bit Length : {}\n".format(len(r[2:]))) # Bit Length
		sys.stdout.write("8 bit coding for example : {}{}{}\n".format(r[:2], (8-len(r[2:]))*"0", r[2:]))
		sys.stdout.write("\n{}\n\n".format("-"*50))



	@classmethod
	def way5(cls):

		sys.stdout.write(".:: Way n°5 - Prompt ::.\n\n")

		for _ in range(0, 3):
			n = int(input("N : "))
			sys.stdout.write("{} => {}\n\n".format(n, Conv.dec2bin(n)))


#Apps.way1()
#Apps.way2()
#Apps.way3()
#Apps.way4()
Apps.way5()


if sys.platform.startswith('win32'):
	os.system("pause")